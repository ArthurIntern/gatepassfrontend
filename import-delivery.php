<?php
ob_start();
$page="Import Delivery";
include_once('header.php');
error_reporting(0);
?>
<?Php
include_once 'includes/db_connect.php';

if($_SESSION['isloggedin']==1)
{
	if(isset($_POST['proceed']))
	{

			$name=$_SESSION["nameloggedin"]; 
			$userName = $_SESSION['nameloggedin'];
            $cont= $_SESSION["contact"];

			$servername2 = "192.168.81.25";
			$username2 = "writer";
			$password2 = "swedenAlsoNyeff47%";
			$db2 = "siginon";


			// Strict conversion of form input data into correct data type
			$awbNum = intval($_POST['AWB']);
			$h_AWB = intval($_POST['h_AWB']);
			$pcs = intval($_POST['pieces']);
			$clrComp = strval($_POST['clearing_comp']);
			$clrAgentNm = strval($_POST['clearingAgentName']);
			$clrAgentID = intval($_POST['clearingAgentID']);
			$clrAgentPhone = intval($_POST['clearingAgentPhone']);
			$stff = strval($_POST['staffname']);
			$rmk = strval($_POST['remarks']);
			$secNm = strval($_POST['securityofficername']);

			try 
			{
						$pdo = new PDO("mysql:host=$servername2;port=3306;db=$db2", $username2, $password2);
				// execute the stored procedure
				//$awbNum,$h_AWB,$pcs,$wgt,$clrAgentNm,$clrAgentID,$clrAgentPhone,$stff,$rmk,$secNm,$secID form input variables
				$sql_query ="call siginon.addImportDelivery(
				$awbNum,'$h_AWB',$pcs,'$clrComp','$clrAgentNm',$clrAgentID,
				$clrAgentPhone,'$stff','$rmk','$secNm',)";
				// call the stored procedure
				$q = $pdo->query($sql_query);
				$q->setFetchMode(PDO::FETCH_ASSOC);
				$message= "New Import Delivery Item Added Successfully!";
			} 
			catch (PDOException $e) 
			{
				die("Error occurred:" . $e->getMessage());
			}

			$_SESSION["message"] ="New Import Delivery Item Added Successfully!";

			header('Location: index.php');
		}


	?>

	<div class="container-fluid">
		<?php
			if(isset($message)){ echo '<div class="alert mt-3 alert-success" role="alert">
			<strong>Note: </strong>
			' .$message. '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button></div>'; }
		?>
		<div class="container-fluid pt-3">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6 p-4">
						<div class="container-fluid p-4">
							<h4 class="text-danger">Details</h4>
							<hr/>
							<form method="POST">
							<div class="form-group">
								<label for="exampleFormControlInput1">AWB</label>
								<input type="number" class="form-control" id="exampleFormControlInput1" placeholder="Enter AWB" name="AWB" required/>
							</div>

							<div class="form-group">
								<label for="exampleFormControlInput1">House AWB</label>
								<input type="text" class="form-control"   placeholder= "Enter House AWB (If Any)" name="h_AWB" />
							</div>
							
							<div class="form-group">
								<label for="exampleFormControlInput1">Number of Pieces</label>
								<div class="form-group">
									<input type="number" class="form-control" id="exampleFormControlInput1" placeholder="Enter Pieces"  name="pieces" required/>
								</div>
							</div>
							
							<div class="form-group">
								<label for="exampleFormControlInput1">Clearing Company Name</label>
								<input type="text" class="form-control"  id="exampleFormControlInput1"  placeholder= "Enter Clearing Company" name="clearing_comp" required />
							</div>

							<div class="form-group">
								<label for="exampleFormControlInput1">Clearing Agent Name</label>
								<input type="text" class="form-control"  placeholder="Enter Clearing Agent Name" name="clearingAgentName" required/>
							</div>

						</div>
					</div>
					<div class="col-md-6 p-4">
						<div class="container-fluid p-4">
							<h4 class="text-danger">Information</h4>
							<hr/>
							
							<div class="form-group">
								<label for="exampleFormControlInput1">Clearing Agent ID</label>
								<div class="form-group">
									<input type="number" class="form-control"  placeholder="Enter Clearing Agent ID Number" name="clearingAgentID" required  minlength="6" maxlength="10"/>
								</div>
							</div>

							<div class="form-group">
								<label for="exampleFormControlInput1">Clearing Agent Phone Number</label>
								<div class="form-group">
									<input type="number" class="form-control" placeholder="Enter Clearing Agent Phone Number" name="clearingAgentPhone" required  minlength="8" maxlength="12"/>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleFormControlInput1">Siginon Staff Name</label>
								<div class="form-group">
									<input type="text" class="form-control"  placeholder="Siginon Staff Present" name="staffname" required/>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleFormControlInput1">Remarks</label>
								<div class="form-group">
									<input type="LANGTEXT" class="form-control"  placeholder="Remarks(If Any)" name="remarks"  />
								</div>
							</div>

							<div class="form-group">
								<label for="exampleFormControlInput1">Security Officer Name</label>
								<div class="form-group">
									<input type="text" class="form-control"  placeholder="Enter Security Officer Name" name="securityofficername" required />
								</div>                          

									<button class="btn btn-danger my-2 my-sm-0" name="proceed" type="submit" formaction = ""><span class="mr-sm-2">SUBMIT</span></button>
							</form>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
} 
else
{
 	echo "<script>alert('Please Log in first');</script>";
	header('Location: index1.php');
}
include_once('footer.php');
?>
