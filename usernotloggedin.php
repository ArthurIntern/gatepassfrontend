<?php
include_once 'includes/db_connect.php';
$page='Register';
ob_start();

 $error= $name_error = $email_error=  $mobile_error  = $cpassword_error='';

    if (isset($_POST['signup'])) {
        $name  = $_POST['name'];
        $email = $_POST['email'];
        $mobile = $_POST['mobile'];
        $password =  $_POST['password'];
        $cpassword =  $_POST['cpassword']; 
    $dateRegistration = $_POST['dateRegistration'];
    
      $query1 = "SELECT * FROM `user` WHERE `email_id` =? ";
      $result = $conn->prepare($query1);
      $result->execute(array($email));
      $row =$result->rowCount();
    //  $fetch =$result->fetch();

    

    if ($row > 0){
      
      $email_error  = "Email already exists! Try another";
          
}

       elseif (!preg_match("/^[a-zA-Z ]+$/",$name)) {
            $name_error = "Name must contain only alphabets and space";
        }
       elseif(!filter_var($email,FILTER_VALIDATE_EMAIL)) {

     
            $email_error = "Please Enter Valid Email ID";
      
      
      
        }
        elseif(strlen($password) < 6) {
            $password_error = "Password must be minimum of 6 characters";
        }       
        elseif(strlen($mobile) < 10) {
            $mobile_error = "Mobile number must be minimum of 10 characters";
        }
       elseif($password != $cpassword) {
            $cpassword_error = "Password and Confirm Password doesn't match";
        }
    
      else 
    {
    
    $hashvalue = password_hash($password, PASSWORD_BCRYPT);
    
    
    
    
    $sql = "INSERT INTO user (full_name, email_id, contact_no,date_registration, password) VALUES (:n,:e,:m,:dt,:p) ";
$q = $conn->prepare($sql);

$q->execute(array(':n'=>$name,':e'=>$email, ':m'=>$mobile, ':dt'=>  $dateRegistration,':p'=>$hashvalue));
  
  
  
        header('Location: Login.php');
    
          
        }
        
    }
?>

<li class="nav-item">
<?php include 'css/css.html'; ?>
  <button type="button" class="btn btn-outline-danger mr-2" data-toggle="modal" id="popupbutton" data-target="#exampleModalCenter">
                Click Here To Start
    </button>
    
<div class="form modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

<ul class="tab-group">
<li class="tab active"><a href="#login">Login</a></li>
<li class="tab"><a href="#signup">Register</a></li>
</ul>
<div class="tab-content">

<div id="login">   
<h1>Welcome to Signinon!</h1>

<form name="login_form" method="post" autocomplete="off">
          
 <div class="field-wrap">
<input type="email" required autocomplete="off" name="email" placeholder="Enter email..."/></div>
          
<div class="field-wrap">
<input type="password" required autocomplete="off" name="pass" placeholder="Password"/> </div>
<div class="top-row">
<div class="field-wrap">
<select class="form-control" name="position">
<option>--Select Role--</option>
<option>User</option>
<option>Admin</option>
</select>
 </div>  
 <div class="field-wrap">
 <button class="button button-block" name="loginbutton" />Log In</button>
</div> 
 </div>      
<div class="form-check">
          <input type="checkbox" class="mt-sm-1 form-check-input" id="exampleCheck1">
          <label class="form-check-label" for="exampleCheck1">Remember me</label>
           </div>   

</form>

</div>
          
<div id="signup">   
          <h1>Register here</h1>
          
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" autocomplete="off">
<div class="field-wrap">
	<input type="hidden" name="dateRegistration" value="<?php echo date("Y-m-d H:i:s"); ?>"/>
<input type="text" required autocomplete="off" name="name"  value="" minlength="5" placeholder="fullname(s)" />
<span class="text-danger"><?php if (isset($name_error)) echo $name_error; ?></span>
</div> 
<div class="top-row">
<div class="field-wrap">
<input type="email" required autocomplete="off" name="email" placeholder="Enter valid Email" maxlength="30" />
<span class="text-danger"><?php if (isset($email_error)) echo $email_error; ?></span>
</div>
<div class="field-wrap">
<input type="number"  autocomplete="off" name="mobile"  value="" min="1"  placeholder="0712000000" maxlength="12"  minlength="10" required />
<span class="text-danger"><?php if (isset($mobile_error)) echo $mobile_error; ?></span>
</div>   
</div>   
<div class="top-row">
<div class="field-wrap">
<input type="password" autocomplete="off"  name="password"  value=""  placeholder="Enter Password" maxlength="10" required/>
  <span class="text-danger"><?php if (isset($password_error)) echo $password_error; ?></span>
</div>
</span>
<div class="field-wrap">
<input type="password" autocomplete="off"  name="cpassword" value="" placeholder="Confirm Password" maxlength="10" required />
 <span class="text-danger"><?php if (isset($cpassword_error)) echo $cpassword_error; ?></span>
</div>
</div>


<button type="submit" class="button button-block" name="signup" value="Register" />Register</button>
</form>

</div>  
        
</div><!-- tab-content -->
      
</div> <!-- /form -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="js/index.js"></script>
</body>
</html>
</li>