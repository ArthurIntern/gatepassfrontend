
<?php
/*Header page*/

session_start();
if(isset($_POST['loginbutton'])){
	include_once 'includes/db_connect.php';	
	$username = $_POST['email'];
	$password = $_POST['pass'];
	

			$sql = "SELECT * FROM `user` WHERE `email_id` =? ";
			$query = $conn->prepare($sql);
			$query->execute(array($username));
			$row = $query->rowCount();

	if ($row > 0) {
     	$fetch = $query->fetch(PDO::FETCH_ASSOC);
	
			$validPassword = password_verify($password, $fetch['password']);
			//$validPassword == $fetch['password'];
			if($validPassword){
					
				$_SESSION["emailloggedin"] = $username;
				$_SESSION["nameloggedin"] = $fetch['full_name'];
				$_SESSION["idloggedin"] = $fetch['user_id'];
				$_SESSION["isloggedin"] = '1';
				$_SESSION["contact"] = $fetch['contact_no'];
				
				 header('Location: index.php');
				
			
			}
			else{
				//out
				$error_msg_login = "Username and email combination are incorrect";
				
		
			}
	}
	else{
		$error_msg_login = "Username/Email does not exist!";
	}
}

?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
	<link href="css/custom.css" rel="stylesheet">
	<link href="web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">
	<script src="js/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script>
   <title>Siginon | <?php echo $page; ?>
</title>
    
<link rel="icon" href="img/logo.jpg" />
  </head>
	<body>

<div class="container-fluid text-danger">

<nav class="navbar navbar-expand-lg  navbar-light bg-light">
		   <a class="navbar-brand" href="index.php">
			<img src="img/logo.jpg"  width="50" height="50" class="img-fluid" alt="" ><strong class="text-danger">
			Siginon Aviation Gate Pass System</strong>
		  </a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		  </button>
		  
		  <div class="collapse navbar-collapse" id="navbarNav">
			 <ul class="navbar-nav mr-auto">
            </ul>
            <div class="col-md-10 navbar">
					<div class="row">
						<div class="col-md-12">
						
	<ul class="nav justify-content-end">
		  <li class="nav-item ">
				<a class="nav-link text-danger" href="index.php"><i class="fas fa-home mr-sm-2 fa-lg"></i>Home<span class="sr-only"></span></a>
			  </li>
	
			   <li class="nav-item">
							<a class="nav-link text-danger" href=""><i class="fas fa-user fa-lg mr-1"></i>Logged in As: </a>
						  </li>
			  

			  
			    <?php
							
						  if(isset($_SESSION['emailloggedin'])){
							  include_once('userloggedin.php');
						  }
						  else{

                        include_once('usernotloggedin.php');
						  }
						  
						  ?>
			</ul>
		  </div>
			
		</nav>
		</div>
		