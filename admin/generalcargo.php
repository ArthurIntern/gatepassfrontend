<?php
ob_start();
error_reporting(E_ALL);
include_once('header.php');
/*Home page*/
$page='General Cargo Export';
$sort_variable = "entry_time";
include_once 'includes/db_connect.php';
if($_SESSION['isadminloggedin']==1){

?>

	<?php
		$results_per_page = 20;

		if (isset($_GET["page"])) 
		{
			$page  = $_GET["page"];
		} 
		else 
		{ 
			$page=1; 
		};
		$start_from = ($page-1) * $results_per_page;

		if((isset($_POST['sort_var'])))
		{
			$sort_variable = $_POST['sort_var'];	
			//$sortType = $_POST['sort_type'];
		}
	?>
<div class="container mb-5">
	<div class="container-fluid pt-3">	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				 	 <a class="navbar-brand" href="#">Search <i class="fas fa-search mt-2 ml-1 fa-lg"></i></a>
					 <a class="navbar-brand" href="#">& Sort <i class="fas fa-random mt-2 ml-1 fa-lg"></i></a>
					  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>
					  <div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto justify-content-end">
					  
					  
					   <li class="nav-item">
							<form method="POST" action="generalcargosearch.php">
								 <input class="form-control mr-sm-2" name="search_Query" type="search" value="<?php if(isset($_SESSION['Query_Search']))echo $_SESSION['Query_Search'];?>" placeholder="Search..." aria-label="Search" pattern=".{3,}" title="Enter 3 or more characters to continue search." autofocus />
								<input type="hidden" name="sort_type" value="ASC" />
							
						 </li>
					    <li class="nav-item">
					    	<button class="btn btn-outline-primary btn-block my-2 my-sm-0 mb-sm-1" name="search_Query_Submit" type="submit">Search<i class="fas fa-angle-right mt-2 ml-1"></i></button>
							</li>
					  </form>
					  
					  
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-left border-right btn btn-link nav-link" name="sort_var" value="entry_time ASC"><span class="text-dark">Date<i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						 </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="entry_time DESC"><span class="text-dark">Date<i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="AWB ASC"><span class="text-dark">AWB <i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						  </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="AWB DESC"><span class="text-dark">AWB <i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						</ul>
					  </div>
					</nav>
	

<?php //Building Query from View base code

$qry = "select
	gc.general_cargo_ID AS 'General Cargo Number',
	gc.entry_time AS 'Date & Time',
	gc.AWB AS 'AWB',
	gc.goods AS 'Goods',
	gc.pieces AS 'Pieces',
	gc.origin_location_abbr AS 'Origin',
	gc.weight AS 'Weight(Kg)',
	gc.destination_abbr AS 'Destination',
	gc.clearing_agent_name AS 'Clearing Agent Name',
	gc.clearing_agent_govt_ID AS 'ID Number',
	gc.clearing_agent_phone AS 'Phone Number',
	gc.shipper_name AS 'Shipper Name',
	gc.security_name AS 'Security Name' 
	from siginon.general_cargo_export AS gc
	ORDER BY 'Date & Time' DESC";

$result = $conn->prepare($qry);
$result->execute();
?>
		<table class="table">
			<thead>
				<tr>
					<th scope="col">General Cargo Number</th>
					<th scope="col">Date & Time</th>
					<th scope="col">AWB</th>
					<th scope="col">Goods</th>
					<th scope="col">Pieces</th>
					<th scope="col">Origin</th>
					<th scope="col">Weight(Kg)</th>
					<th scope="col">Destination</th>
					<th scope="col">Clearing Agent Name</th>
					<th scope="col">ID Number</th>
					<th scope="col">Phone Number</th>
					<th scope="col">Shipper Name</th>
					<th scope="col">Security Name</th>

				</tr>
			</thead>
			<tbody>
				<?php
				$counter = 1;
				while($got = $result->fetch())
				{	
					?>

						<tr>
							<th scope="row"><?php echo $counter;?></th>
							<td><?php echo $got['General Cargo Number'];?></td>
							<td><?php echo $got['Date & Time'];?></td>
							<td><?php echo $got['AWB'];?></td>
							<td><?php echo $got['Goods'];?></td>
							<td><?php echo $got['Pieces'];?></td>
							<td><?php echo $got['Origin'];?></td>
							<td><?php echo $got['Weight(Kg)'];?></td>
							<td><?php echo $got['Destination'];?></td>
							<td><?php echo $got['Clearing Company'];?></td>
							<td><?php echo $got['Clearing  agent Name'];?></td>
							<td><?php echo $got['ID Number'];?></td>
							<td><?php echo $got['Phone Number'];?></td>
							<td><?php echo $got['Shipper Name'];?></td>
							<td><?php echo $got['Security Name'];?></td>
							<td>
								<form method="POST" class="footer_list">
									<input type="hidden" name="product_Query_Page" value="<?php echo $got['general_cargo_ID '] ?>"/>
								</form>
							</td>
						</tr>
					<?php
					$counter = $counter + 1; 
				}
				?>
			</tbody>
		</table>
		  
		  
		  
		  
		  <?php

	$query = $conn->prepare("select gc.general_cargo_ID AS 'General Cargo Number',
	gc.entry_time AS 'Date & Time',
	gc.AWB AS 'AWB',
	gc.goods AS 'Goods',
	gc.pieces AS 'Pieces',
	gc.origin_location_abbr AS 'Origin',
	gc.weight AS 'Weight(Kg)',
	gc.destination_abbr AS 'Destination',
	gc.clearing_agent_name AS 'Clearing Agent Name',
	gc.clearing_agent_govt_ID AS 'ID Number',
	gc.clearing_agent_phone AS 'Phone Number',
	gc.shipper_name AS 'Shipper Name',
	gc.security_name AS 'Security Name' 
	from siginon.general_cargo_export as gc; ");
		  //$sql = "SELECT userId AS total FROM users ";
    //  $query = $conn->prepare($sql);

      $query ->execute();
      $row = $query->rowCount();
	
	$total_pages = ceil($row / $results_per_page); // calculate total pages with results

for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
         echo " <a href='generalcargo.php?page=".$i."'";

            if ($i==$page)  echo " class='curPage'";
            echo ">".$i."</a> "; 
	
	
}
		?>
	</div>
</div>




<?php
	
include_once('footer.php');
}
else{
	header('Location: login.php');
}
?>
