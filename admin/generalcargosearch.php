<?php
ob_start();
include_once('header.php');
/*Home page*/
$page='searchresults';
$sort_variable = "entry_time";
include_once 'includes/db_connect.php';
if($_SESSION['isadminloggedin']==1){

?>

<?php

	$results_per_page = 20;
	
if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
$start_from = ($page-1) * $results_per_page;

if((isset($_POST['sort_var']))){
	$sort_variable = $_POST['sort_var'];	
	//$sortType = $_POST['sort_type'];
}

	if(((isset($_SESSION['search_Query']))||(isset($_POST['search_Query'])))){

	if(isset($_POST['search_Query'])){
		$_SESSION['search_Query'] = $_POST['search_Query'];
		
		$var1 = $_SESSION['search_Query'];
		
	}
	else{
		$var1 = $_SESSION['search_Query'];
		
	}
	
	$var1 = preg_replace("#[^0-9a-z@ .]#i","",$var1);
	$_SESSION['Query_Search'] = $var1;
	
		

	$look_for_search=$conn->prepare("SELECT * FROM general_cargo_export WHERE general_cargo_ID Like '%$var1%' OR entry_time Like '%$var1%' OR AWB Like '%$var1%'  OR goods Like '%$var1%'  OR pieces Like '%$var1%' OR  origin_location_abbr Like '%$var1%' OR weight Like '%$var1%'  OR   destination_abbr Like '%$var1%'  OR   shipping_company Like '%$var1%'  OR   shipper_name Like '%$var1%' OR   security_name Like '%$var1%' OR   security_name Like '%$var1%' OR   security_govt_ID_number Like '%$var1%' OR   shipping_company Like '%$var1%' ORDER BY $sort_variable  LIMIT $start_from, ".$results_per_page); 
		
	$look_for_search->execute();
		
	
	$count =  $look_for_search->rowCount();
	if($count == 0){
		$message = 'Your query "' .$var1. '" yield no results! Try again with a different search parameters.';
	}
	else{
	}
	if(isset($_POST['complete'])){
	
     	$id = $_POST['fstatus'];
		$complete="UPDATE general_cargo_export  SET acceptance_status = 'ACCEPTED'  WHERE general_cargo_ID = '$id'";
		$q = $conn->prepare($complete);
		$q->execute();

	}  
	
	if(isset($_POST['pending'])){
				
				
   	$id = $_POST['fstatus'];
		$uncleared="UPDATE general_cargo_export  SET acceptance_status = 'NOT ACCEPTED'  WHERE general_cargo_ID = '$id'";
		$q = $conn->prepare($uncleared);
		$q->execute();

	}  
	
?>
<div class="container mb-5">
	<div class="container-fluid pt-3">

	
	<?php
		if(isset($message)){ echo '<div class="alert alert-primary" role="alert">
		  <strong>Note: </strong>
			' .$message. '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
  </button></div>'; }
		?>
</div>
	<?php echo '<h3 class="font-weight-light">' .$count. ' results found! for your query "'.$var1.'"</h3><br/>';?>
	
	
	
	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				 	 <a class="navbar-brand" href="#">Search <i class="fas fa-search mt-2 ml-1 fa-lg"></i></a>
					 <a class="navbar-brand" href="#">& Sort <i class="fas fa-random mt-2 ml-1 fa-lg"></i></a>
					  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>
					  <div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto justify-content-end">
					  
					  
					   <li class="nav-item">
							<form method="POST" action="knowncargosearch.php">
								 <input class="form-control mr-sm-2" name="search_Query" type="search" value="<?php if(isset($_SESSION['Query_Search']))echo $_SESSION['Query_Search'];?>" placeholder="Search..." aria-label="Search" pattern=".{3,}" title="Enter 3 or more characters to continue search." autofocus />
								<input type="hidden" name="sort_type" value="ASC" />
							
						 </li>
					    <li class="nav-item">
					    	<button class="btn btn-outline-primary btn-block my-2 my-sm-0 mb-sm-1" name="search_Query_Submit" type="submit">Search<i class="fas fa-angle-right mt-2 ml-1"></i></button>
							</li>
					  </form>
					  
					  
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-left border-right btn btn-link nav-link" name="sort_var" value="entry_datetime ASC"><span class="text-dark">Date<i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						 </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="entry_datetime DESC"><span class="text-dark">Date<i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="AWB ASC"><span class="text-dark">AWB<i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						  </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="AWB DESC"><span class="text-dark">AWB<i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						</ul>
					  </div>
					</nav>
	

		
		<table class="table">
		  <thead>
			<tr>
				
				
				

				

			  <th scope="col">No.</th>
  <th scope="col">entry_time</th>
			   <th scope="col">AWB</th>
			  <th scope="col">goods</th>
			  	   <th scope="col">pieces</th>
			 <th scope="col">origin_location_abbr</th>
			    <th scope="col">weight</th>
			   <th scope="col">destination_abbr</th>
			  <th scope="col">shipping_company</th>
			   <th scope="col">shipper_name</th>
			 <th scope="col">security_name</th>
			  
			  
		    <th scope="col">security_govt_ID_number</th>
		   <th scope="col">acceptance_status</th>
         
				
			
			  	
			</tr>
		  </thead>
		  <tbody
		    >
		  <?php
		  $counter = 1;
		  while($got = $look_for_search->fetch()){	
		  ?>
		  
		  <tr>
	
			  

				

			  <th scope="row"><?php echo $counter;?></th>
			  <td><?php echo $got['entry_time'];?></td>
			    <td><?php echo $got['AWB'];?></td>
			  <td><?php echo $got['goods'];?></td>
			  	  <td><?php echo $got['pieces'];?></td>
			  <td > <?php echo $got['origin_location_abbr'];?></td>
			   <td><?php echo $got['weight'];?></td>
			   <td><?php echo $got['destination_abbr'];?></td>
			   <td > <?php echo $got['shipping_company'];?></td>
			   <td><?php echo $got['shipper_name'];?></td>
			   <td><?php echo $got['security_name'];?></td>
			  
			  
		   <td><?php echo $got['security_govt_ID_number'];?></td>
	

			  <form action="" method="POST" class="footer_list">
				<input type="hidden" name="product_Query_Page" value="<?php echo $got['general_cargo_ID '] ?>"/>

			</form></td>
			</tr>
		  <?php
		  $counter = $counter + 1; 
		  }
			  ?>
		  </tbody>
		  </table>
		  
		  
		  
		  <?php
	
      $row = $look_for_search->rowCount();
	
	$total_pages = ceil($row / $results_per_page); // calculate total pages with results
  
for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
         echo " <a href='generacargosearch.php?page=".$i."'";

            if ($i==$page)  echo " class='curPage'";
            echo ">".$i."</a> "; 
	
	
}
		?>
	</div>
</div>




<?php
	
include_once('footer.php');
}
}
else{
	header('Location: login.php');
}
?>
