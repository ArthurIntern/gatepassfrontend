<?php
ob_start();
include_once('header.php');
/*Home page*/
$page='searchresults';
$sort_variable = "delivery_datetime";
include_once 'includes/db_connect.php';
if($_SESSION['isadminloggedin']==1){

?>

<?php

	$results_per_page = 20;
	
	
if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
$start_from = ($page-1) * $results_per_page;

if((isset($_POST['sort_var']))){
	$sort_variable = $_POST['sort_var'];	
	//$sortType = $_POST['sort_type'];
}

		
	if(((isset($_SESSION['search_Query']))||(isset($_POST['search_Query'])))){

	if(isset($_POST['search_Query'])){
		$_SESSION['search_Query'] = $_POST['search_Query'];
		
		$var1 = $_SESSION['search_Query'];
		
	}
	else{
		$var1 = $_SESSION['search_Query'];
		
	}
	
	$var1 = preg_replace("#[^0-9a-z@ .]#i","",$var1);
	$_SESSION['Query_Search'] = $var1;
	
	
		
	$look_for_search=$conn->prepare("SELECT * FROM import_delivery WHERE import_delivery_ID Like '%$var1%' OR AWB Like '%$var1%' OR House_AWB Like '%$var1%'  OR pieces Like '%$var1%'  OR clearing_agent_name Like '%$var1%' OR  clearing_agent_phone Like '%$var1%' OR siginon_staff_name Like '%$var1%'  OR   clearing_agent_phone Like '%$var1%' ORDER BY $sort_variable  LIMIT $start_from, ".$results_per_page); 
		
	$look_for_search->execute();
		
	
	$count =  $look_for_search->rowCount();
	if($count == 0){
		$message = 'Your query "' .$var1. '" yield no results! Try again with a different search parameters.';
	}
	else{
	}
	if(isset($_POST['complete'])){
	
     	$id = $_POST['fstatus'];
		$complete="UPDATE import_delivery  SET remarks = 'CLEARED'  WHERE import_delivery_ID = '$id'";
		$q = $conn->prepare($complete);
		$q->execute();

		
		
		
	}  
	
	if(isset($_POST['pending'])){
				
				
   	$id = $_POST['fstatus'];
		$uncleared="UPDATE import_delivery  SET remarks = 'NOT YET CLEARED'  WHERE import_delivery_ID = '$id'";
		$q = $conn->prepare($uncleared);
		$q->execute();

	}  
	


	
?>
<div class="container mb-5">
	<div class="container-fluid pt-3">
	
	
	
	<?php
		if(isset($message)){ echo '<div class="alert alert-primary" role="alert">
		  <strong>Note: </strong>
			' .$message. '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
  </button></div>'; }
		?>
</div>
	<?php echo '<h3 class="font-weight-light">' .$count. ' results found! for your query "'.$var1.'"</h3><br/>';?>
	
	
	
	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				 	 <a class="navbar-brand" href="#">Search <i class="fas fa-search mt-2 ml-1 fa-lg"></i></a>
					 <a class="navbar-brand" href="#">& Sort <i class="fas fa-random mt-2 ml-1 fa-lg"></i></a>
					  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>
					  <div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto justify-content-end">
					  
					  
					   <li class="nav-item">
							<form method="POST" action="importcargosearch.php">
								 <input class="form-control mr-sm-2" name="search_Query" type="search" value="<?php if(isset($_SESSION['Query_Search']))echo $_SESSION['Query_Search'];?>" placeholder="Search..." aria-label="Search" pattern=".{3,}" title="Enter 3 or more characters to continue search." autofocus />
								<input type="hidden" name="sort_type" value="ASC" />
							
						 </li>
					    <li class="nav-item">
					    	<button class="btn btn-outline-primary btn-block my-2 my-sm-0 mb-sm-1" name="search_Query_Submit" type="submit">Search<i class="fas fa-angle-right mt-2 ml-1"></i></button>
							</li>
					  </form>
					  
					  
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-left border-right btn btn-link nav-link" name="sort_var" value="delivery_datetime ASC"><span class="text-dark">Date<i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						 </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="delivery_datetime DESC"><span class="text-dark">Date<i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="pieces ASC"><span class="text-dark">PIECES<i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						  </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="pieces DESC"><span class="text-dark">PIECES<i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						</ul>
					  </div>
					</nav>
	

		<table class="table">
		  <thead>
			<tr
			   <tr
		
			>
			  <th scope="col">Sr. no.</th>
			  <th scope="col">Trans.Id/sellId</th>
			  <th scope="col">Fin.T.Id/BuyId</th>
			  <th scope="col">Seller</th>
			  <th scope="col">Date Bidded</th>
			
			  <th scope="col">Buy.Pri</th>
			  	  <th scope="col">Sell.Pri</th>
			  	  	  <th scope="col">Days</th>
		  	  	  	     <th scope="col">Start</th>
			  	  	  	  <th scope="col">Timetosell</th>
	  	  	  	  	  <th scope="col">Status</th>
		  	  	  	  	  <th scope="col">Buyer</th>
		  	  	  	  	  
			  	  	  	    <th scope="col">Buyer Cont.</th>	  
			  	  	  	  	  	  <th scope="col">Final Status</th>
			  	  	  	  	  	  
			</tr>
		  </thead>
		  <tbody
		    >
		  <?php
		  $counter = 1;
		
			    while($got = $look_for_search->fetch()){	
		  ?>
		  
		  <tr>
		
			  <th scope="row"><?php echo $counter;?></th>
			  <td><?php echo $got['delivery_datetime'];?></td>
			    <td><?php echo $got['AWB'];?></td>
			  <td><?php echo $got['House_AWB'];?></td>
			  	  <td><?php echo $got['pieces'];?></td>
			  <td > <?php echo $got['weight'];?></td>
			   <td><?php echo $got['clearing_agent_name'];?></td>
			   <td><?php echo $got['clearing_agent_govt_ID'];?></td>
			  
			  
		   <td><?php echo $got['clearing_agent_phone'];?></td>
		   <td><?php echo $got['siginon_staff_name'];?></td>
           <td><?php echo $got['security_name'];?></td>
	       <td><?php echo $got['security_govt_ID_number'];?></td>
			    <td><?php echo $got['remarks'];?></td>

			   <td><strong><?php echo ' '.$got['remarks'].'<form method="POST" ><input type="hidden" name="fstatus" value="'.$got['import_delivery_ID'].'">
			   <button class=" btn btn-primary" type="submit"  name="complete" ><i class=" fas fa-check-square mr-2"></i>CLEAR</button>
	
			   <button class=" btn btn-danger" type="submit"  name="pending" ><i class=" fas fa-ban mr-2"></i>UNCLEAR</button>
			   </form>';?></strong></td>
			  <td>
			  <form action="" method="POST" class="footer_list">
				<input type="hidden" name="product_Query_Page" value="<?php echo $got['import_delivery_ID'] ?>"/>

			</form></td>
			</tr>
		  <?php
		  $counter = $counter + 1; 
		  }
			  ?>
		  </tbody>
		  </table>
		  
		  
		  
		  
		  <?php
	
      $row = $look_for_search->rowCount();
	
	$total_pages = ceil($row / $results_per_page); // calculate total pages with results
  
for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
         echo " <a href='importcargosearch.php?page=".$i."'";

            if ($i==$page)  echo " class='curPage'";
            echo ">".$i."</a> "; 
	
	
}
		?>
	</div>
</div>




<?php
	
include_once('footer.php');
}
}
else{
	header('Location: login.php');
}
?>
