<?php


$page='Register';
//ob_start();

//include_once('header.php');


include_once 'includes/db_connect.php';
 $error= $name_error = $email_error=  $mobile_error  = $cpassword_error='';

    if (isset($_POST['signup'])) {
        $name =  $_POST['name'];
        $email = $_POST['email'];
        $mobile = $_POST['mobile'];
        $password = $_POST['password'];
        $cpassword = $_POST['cpassword']; 
		$dateRegistration =$_POST['dateRegistration'];
	

		
		$query1="SELECT user_id FROM user WHERE email_id= ?";
		$result =$query = $conn->prepare($query1);
		$result->execute(array($email));
		$num_rows =$query->rowCount();

		if ($num_rows > 0){
			
			$email_error  = "Email already exists! Try another";
          
}

       elseif (!preg_match("/^[a-zA-Z ]+$/",$name)) {
            $name_error = "Name must contain only alphabets and space";
        }
       elseif(!filter_var($email,FILTER_VALIDATE_EMAIL)) {

	   
            $email_error = "Please Enter Valid Email ID";
			
			
			
        }
        elseif(strlen($password) < 6) {
            $password_error = "Password must be minimum of 6 characters";
        }       
        elseif(strlen($mobile) < 10) {
            $mobile_error = "Mobile number must be minimum of 10 characters";
        }
       elseif($password != $cpassword) {
            $cpassword_error = "Password and Confirm Password doesn't match";
        }
		
      else 
	  {
		
		$hashvalue = password_hash($password, PASSWORD_BCRYPT);
		
		  

			$sql = "INSERT INTO admin_user  (name, email,  contactNo,dateRegistration, password) VALUES (:f,:e,:c,:d,:p)";
		$q = $conn->prepare($sql);

$q->execute(array(':f'=>$name, ':e'=>$email,':c'=>$mobile,':d'=>$dateRegistration, ':p'=>$hashvalue ));

		
					
				header('Location: addadmin.php? message= Admin '.$name.' Has Been Added Successfully');
		
          
        }
        
    }
?>

   <div class="mt-5 container">
	<h2 class="font-weight-light text-center mb-3">Add Admin:</h2>
	
	<div class="row">
		<div class="col-md-3">
		</div>
		<div class="col-md-6 rounded alert alert-dark">
			<div class="p-3 font-weight-light">
		
								<?php
	if(isset($_GET['message'])){ echo '<div class="alert mt-3 alert-success" role="alert">
			  <strong>Success: </strong>
				'.$_GET['message'].'<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	  </button></div>'; }
	   unset($_GET['message']);
				?>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

                    <div class="form-group">
					<input type="hidden" name="dateRegistration" value="<?php echo date("Y-m-d H:i:s"); ?>"/>
                        <label> Full Names</label>
                        <input type="text" name="name" class="form-control" value="" minlength="5" placeholder="First name Second name"  maxlength="50" required="">
                        <span class="text-danger"><?php if (isset($name_error)) echo $name_error; ?></span>
                    </div>

                    <div class="form-group ">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" value=""  placeholder="Enter valid Email" maxlength="30" required="">
                        <span class="text-danger"><?php if (isset($email_error)) echo $email_error; ?></span>
                    </div>

                    <div class="form-group">
                        <label>Mobile</label>
                        <input type="number" name="mobile" class="form-control" value="" min="1"  placeholder="0712000000" maxlength="12"  minlength="10" required>
                        <span class="text-danger"><?php if (isset($mobile_error)) echo $mobile_error; ?></span>
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" value=""  placeholder="Enter Password" maxlength="10" required>
                        <span class="text-danger"><?php if (isset($password_error)) echo $password_error; ?></span>
                    </div>  

                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="cpassword" class="form-control" value="" placeholder="Confirm Password" maxlength="10" required>
                        <span class="text-danger"><?php if (isset($cpassword_error)) echo $cpassword_error; ?></span>
                    </div>

							<div class="text-center">
					<span class="mr-3 ml-3"> <input type="submit" class="btn btn-primary" name="signup" value="Register"></span>							
					</div>
					
					
                </form>
          
			</div>
		
		</div>
		<div class="col-md-3">
		</div>
	</div>
</div>
<?php
include_once('footer.php');
?>

