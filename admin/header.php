<?php
/*Header page*/
session_start();
$page = 'header';
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" crossorigin="anonymous">
	<link href="../css/custom.css" rel="stylesheet">
	<link href="css/custom.css" rel="stylesheet">
	<link href="../web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">
    <title>Siginon</title>
    <link rel="icon" href="../img/logo.jpg" />
  </head>
	<body>
		
<div class="container-fluid">

<nav class="navbar navbar-expand-lg  navbar-light bg-light">
		   <a class="navbar-brand" href="index.php">
			<img src="../img/logo.jpg"  width="50" height="50" class="d-inline-block align-top" alt="" >
			Siginon
		  </a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		  </button>
		<?php
			if(isset($_SESSION['adminloggedin'])){
			echo '
		  <div class="collapse navbar-collapse" id="navbarNav">
			 <ul class="navbar-nav mr-auto">
            </ul>
			<ul class="navbar-nav">
			 <strong></strong>
		     <li class="nav-item active">
				  <a class="nav-link" href="index.php"><i class="fas fa-home mr-sm-2 fa-lg"></i>Unknown Cargo<span class="sr-only"></span></a>
			  </li>
				<li class="nav-item active">
			<a class="nav-link" href="knowncargo.php"><i class="fas fa-plus mr-sm-2 fa-lg"></i>known cargo <span class="sr-only"></span></a>
			  </li>



<li class="nav-item active">
			<a class="nav-link" href="importcargo.php"><i class="fas fa-plus mr-sm-2 fa-lg"></i>import cargo <span class="sr-only"></span></a>
			  </li>
		  <li class="nav-item active">
			  <a class="nav-link" href="generalcargo.php"><i class="fas fa-plus mr-sm-2 fa-lg"></i>General Cargo export<span class="sr-only"></span></a>
			  </li>
			  
			   <li class="nav-item active">
			  <a class="nav-link" href="exportcargodelivery.php"><i class="fas fa-plus mr-sm-2 fa-lg"></i>Export Cargo Delivey<span class="sr-only"></span></a>
			  </li>

			  <li class="nav-item active">
				  <a class="nav-link" href="adduser.php"><i class="fas fa-cogs mr-sm-2 fa-lg"></i>Add user<span class="sr-only">(current)</span></a>
			  </li>
		
          <li class="nav-item active">
				<a class="nav-link" href="users.php"><i class="fas fa-users mr-sm-2 fa-lg"></i>Users <span class="sr-only"></span></a>
			  </li>
		<li class="nav-item active">
			<a class="nav-link" href="addadmin.php"><i class="fas fa-plus mr-sm-2 fa-lg"></i>Add Admin <span class="sr-only"></span></a>
			  </li>
			
			
			  <li class="nav-item active">
				<a class="nav-link" href="Profile.php"><i class="fas fa-user mr-sm-2 fa-lg"></i>'.$_SESSION['adminloggedin'].' <span class="sr-only"></span></a>
			  </li>
			  <li class="nav-item active">
				<a class="nav-link" href="includes/logout.php"><i class="fas fa-sign-out-alt mr-sm-2 fa-lg"></i>Logout</a>
			  </li>
			</ul>
		  </div>
			';}
		  ?>
		</nav>
		</div>
		