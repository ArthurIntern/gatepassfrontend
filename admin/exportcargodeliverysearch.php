<?php
ob_start();
include_once('header.php');
/*Home page*/
$page='searchresults';
$sort_variable = "entry";
include_once 'includes/db_connect.php';
if($_SESSION['isadminloggedin']==1){

?>

<?php

	$results_per_page = 20;
	
if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
$start_from = ($page-1) * $results_per_page;

if((isset($_POST['sort_var']))){
	$sort_variable = $_POST['sort_var'];	
	//$sortType = $_POST['sort_type'];
}

	if(((isset($_SESSION['search_Query']))||(isset($_POST['search_Query'])))){

	if(isset($_POST['search_Query'])){
		$_SESSION['search_Query'] = $_POST['search_Query'];
		
		$var1 = $_SESSION['search_Query'];
		
	}
	else{
		$var1 = $_SESSION['search_Query'];
		
	}
	
	$var1 = preg_replace("#[^0-9a-z@ .]#i","",$var1);
	$_SESSION['Query_Search'] = $var1;
	
		



	$look_for_search=$conn->prepare("SELECT * FROM export_cargo_delivery WHERE export_cargo_delivery_ID Like '%$var1%' OR entry Like '%$var1%' OR AWB Like '%$var1%'  OR goods Like '%$var1%'  OR pieces Like '%$var1%' OR  location_abbr Like '%$var1%' OR weight Like '%$var1%'  OR   destination_abbr Like '%$var1%'  OR   flight Like '%$var1%'  OR   container Like '%$var1%' OR   TimeOut Like '%$var1%' OR   container Like '%$var1%' OR   TimeOut Like '%$var1%' OR   export_driver_govt_ID Like '%$var1%' OR   pass_card Like '%$var1%' OR   clerk_name Like '%$var1%' ORDER BY $sort_variable  LIMIT $start_from, ".$results_per_page); 
		
	$look_for_search->execute();
		
	
	$count =  $look_for_search->rowCount();
	if($count == 0){
		$message = 'Your query "' .$var1. '" yield no results! Try again with a different search parameters.';
	}
	else{
	}
	if(isset($_POST['complete'])){
	
     	$id = $_POST['fstatus'];
		$complete="UPDATE export_cargo_delivery  SET export_status = 'ACCEPTED'  WHERE export_cargo_ID= '$id'";
		$q = $conn->prepare($complete);
		$q->execute();

	}  
	
	if(isset($_POST['pending'])){
				
				
   	$id = $_POST['fstatus'];
		$uncleared="UPDATE export_cargo_delivery  SET export_status = 'NOT ACCEPTED'  WHERE export_cargo_ID= '$id'";
		$q = $conn->prepare($uncleared);
		$q->execute();

	}  
	
?>
<div class="container mb-5">
	<div class="container-fluid pt-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="exportcargosearch.php">Home</a></li>
		<li class="breadcrumb-item">Search results</li>
	  </ol>
	</nav>
	
	
	<?php
		if(isset($message)){ echo '<div class="alert alert-primary" role="alert">
		  <strong>Note: </strong>
			' .$message. '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
  </button></div>'; }
		?>
</div>
	<?php echo '<h3 class="font-weight-light">' .$count. ' results found! for your query "'.$var1.'"</h3><br/>';?>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				 	 <a class="navbar-brand" href="#">Search <i class="fas fa-search mt-2 ml-1 fa-lg"></i></a>
					 <a class="navbar-brand" href="#">& Sort <i class="fas fa-random mt-2 ml-1 fa-lg"></i></a>
					  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>
					  <div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto justify-content-end">
					  
					  
					   <li class="nav-item">
							<form method="POST" action="exportcargodeliverysearch.php">
								 <input class="form-control mr-sm-2" name="search_Query" type="search" value="<?php if(isset($_SESSION['Query_Search']))echo $_SESSION['Query_Search'];?>" placeholder="Search..." aria-label="Search" pattern=".{3,}" title="Enter 3 or more characters to continue search." autofocus />
								<input type="hidden" name="sort_type" value="ASC" />
							
						 </li>
					    <li class="nav-item">
					    	<button class="btn btn-outline-primary btn-block my-2 my-sm-0 mb-sm-1" name="search_Query_Submit" type="submit">Search<i class="fas fa-angle-right mt-2 ml-1"></i></button>
							</li>
					  </form>
					  
					  
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-left border-right btn btn-link nav-link" name="sort_var" value="entry ASC"><span class="text-dark">Date<i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						 </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="entry DESC"><span class="text-dark">Date<i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="AWB ASC"><span class="text-dark">AWB<i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						  </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="AWB DESC"><span class="text-dark">AWB<i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						</ul>
					  </div>
					</nav>
	

		
		<table class="table">
		  <thead>
			<tr>
				
						

		

			  <th scope="col">No.</th>
			  <th scope="col">General Cargo ID</th>
  <th scope="col">entry_datetime</th>
			   <th scope="col">AWB</th>
			  <th scope="col">goods</th>
			  	   <th scope="col">pieces</th>
			 <th scope="col">location_abbr</th>
			    <th scope="col">weight</th>
			   <th scope="col">destination_abbr</th>
			  <th scope="col">flight</th>
			   <th scope="col">container</th>
			 <th scope="col">TimeOut</th>
			  
			  
		    <th scope="col">export_driver_govt_ID</th>
		   <th scope="col">pass_card</th>
         
				<th scope="col">clerk_name</th>
		   <th scope="col">export_status</th>
			
			  	
			</tr>
		  </thead>
		  <tbody
		    >
		  <?php
		  $counter = 1;
		  while($got = $look_for_search->fetch()){	
		  ?>
		  
		  <tr>

			  <th scope="row"><?php echo $counter;?></th>
			  <td><?php echo $got['entry'];?></td>
			    <td><?php echo $got['AWB'];?></td>
			  <td><?php echo $got['goods'];?></td>
			  	  <td><?php echo $got['pieces'];?></td>
			  <td > <?php echo $got['location_abbr'];?></td>
			   <td><?php echo $got['weight'];?></td>
			   <td><?php echo $got['destination_abbr'];?></td>
			   <td > <?php echo $got['flight'];?></td>
			   <td><?php echo $got['container'];?></td>
			   <td><?php echo $got['TimeOut'];?></td>
			  
		   <td><?php echo $got['export_driver_govt_ID'];?></td>
			  
			  	   <td><?php echo $got['pass_card'];?></td>
			   <td><?php echo $got['clerk_name'];?></td>
			  
		   <td><?php echo $got['clerk_govt_ID'];?></td>
			  
	
			</tr>
		  <?php
		  $counter = $counter + 1; 
		  }
			  ?>
		  </tbody>
		  </table>
		  
		  
		  
		  <?php
	
      $row = $look_for_search->rowCount();
	
	$total_pages = ceil($row / $results_per_page); // calculate total pages with results
  
for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
         echo " <a href='exportcargodeliverysearch.php?page=".$i."'";

            if ($i==$page)  echo " class='curPage'";
            echo ">".$i."</a> "; 
	
	
}
		?>
	</div>
</div>




<?php
	
include_once('footer.php');
}
}
else{
	header('Location: login.php');
}
?>
