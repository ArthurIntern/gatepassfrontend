<?php
ob_start();
include_once('header.php');
/*Home page*/
$page='Export Cargo Delivery View';
$sort_variable = "entry";
include_once 'includes/db_connect.php';
if($_SESSION['isadminloggedin']==1){

?>


	<?php

	$results_per_page = 20;

	if (isset($_GET["page"])) 
		{
			$page  = $_GET["page"];
		} 
	else 
		{ 
			$page=1;
		};
	$start_from = ($page-1) * $results_per_page;

	if((isset($_POST['sort_var'])))
	{
		$sort_variable = $_POST['sort_var'];	
		//$sortType = $_POST['sort_type'];
	}

				$result = $conn->prepare("SELECT * FROM export_cargo_delivery  ORDER BY $sort_variable  LIMIT $start_from, ".$results_per_page );

				$result->execute();
		

	?>

<div class="container mb-5">
	<div class="container-fluid pt-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="exportcargosearch.php">Home</a></li>
		<li class="breadcrumb-item">Search results</li>
	  </ol>
	</nav>
	
	
	<?php
		if(isset($message)){ echo '<div class="alert alert-primary" role="alert">
		  <strong>Note: </strong>
			' .$message. '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
  </button></div>'; }
		?>
</div>
	<?php echo '<h3 class="font-weight-light">' .$count. ' results found! for your query "'.$var1.'"</h3><br/>';?>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				 	 <a class="navbar-brand" href="#">Search <i class="fas fa-search mt-2 ml-1 fa-lg"></i></a>
					 <a class="navbar-brand" href="#">& Sort <i class="fas fa-random mt-2 ml-1 fa-lg"></i></a>
					  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>
					  <div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto justify-content-end">
					  
					  
					   <li class="nav-item">
							<form method="POST" action="exportcargosearch.php">
								 <input class="form-control mr-sm-2" name="search_Query" type="search" value="<?php if(isset($_SESSION['Query_Search']))echo $_SESSION['Query_Search'];?>" placeholder="Search..." aria-label="Search" pattern=".{3,}" title="Enter 3 or more characters to continue search." autofocus />
								<input type="hidden" name="sort_type" value="ASC" />
							
						 </li>
					    <li class="nav-item">
					    	<button class="btn btn-outline-primary btn-block my-2 my-sm-0 mb-sm-1" name="search_Query_Submit" type="submit">Search<i class="fas fa-angle-right mt-2 ml-1"></i></button>
							</li>
					  </form>
					  
					  
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-left border-right btn btn-link nav-link" name="sort_var" value="entry_datetime ASC"><span class="text-dark">Date<i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						 </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="entry_datetime DESC"><span class="text-dark">Date<i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="AWB ASC"><span class="text-dark">AWB<i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						  </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="AWB DESC"><span class="text-dark">AWB<i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						</ul>
					  </div>
					</nav>
	

		
<table class="table">
			<thead>
				<tr>
				
				
					<th scope="col">Entry Date</th>
					<th scope="col">AWB</th>
					<th scope="col">Goods</th>
					<th scope="col">Pieces</th>
					<th scope="col">Current Location</th>
					<th scope="col">Weight(Kg)</th>
					<th scope="col">Destination</th>
					<th scope="col">Flight</th>
					<th scope="col">Uld</th>
					<th scope="col">Exit Date</th>
					<th scope="col">Export Driver ID</th>
					<th scope="col">Export Driver Name</th>
					<th scope="col">Pass Card</th>
					<th scope="col">Clerk Name</th>	
					<th scope="col">security Name</th>			
				</tr>
			</thead>
			<tbody>
				<?php
					$counter = 1;
					while($got = $result->fetch())
					{	
				?>
					<tr>
						<th scope="row"><?php echo $counter;?></th>
							<td><?php echo $got['General Cargo ID'];?></td>
							<td><?php echo $got['Entry Date & Time'];?></td>
							<td><?php echo $got['AWB'];?></td>
							<td><?php echo $got['Goods'];?></td>
							<td><?php echo $got['Pieces'];?></td>
							<td><?php echo $got['Current Location'];?></td>
							<td><?php echo $got['Weight(Kg)'];?></td>
							<td><?php echo $got['Destination'];?></td>
							<td><?php echo $got['Flight'];?></td>
							<td><?php echo $got['Uld'];?></td>
							<td><?php echo $got['Exit Date & Time'];?></td>
							<td><?php echo $got['Export Driver ID'];?></td>
							<td><?php echo $got['Export Driver Name'];?></td>
							<td><?php echo $got['Pass Card'];?></td>
							<td><?php echo $got['Clerk Name'];?></td>
							<td><?php echo $got['security Name'];?></td>
							<td>
								<form action="" method="POST" class="footer_list">
									<input type="hidden" name="product_Query_Page" value="<?php echo $got['export_cargo_ID'] ?>"/>
								</form>
							</td>
					</tr>
				<?php
						$counter = $counter + 1;
					}
				?>
			</tbody>


</table>
		  
		  
		  
		  <?php
	
      $row = $look_for_search->rowCount();
	
	$total_pages = ceil($row / $results_per_page); // calculate total pages with results
  
for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
         echo " <a href='exportcargo.php?page=".$i."'";

            if ($i==$page)  echo " class='curPage'";
            echo ">".$i."</a> "; 
	
	
}
		?>
	</div>
</div>




<?php
	
include_once('footer.php');
}

else{
	header('Location: login.php');
}
?>
