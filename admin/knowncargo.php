<?php
ob_start();
error_reporting(E_ALL);
include_once('header.php');
/*Home page*/
$page='Known Cargo View';
$sort_variable = "entry_datetime";
include_once 'includes/db_connect.php';
if($_SESSION['isadminloggedin']==1){

?>

	<?php

	$results_per_page = 20;

	if (isset($_GET["page"])) 
		{
			$page  = $_GET["page"];
		} 
	else 
		{ 
		$page=1; 
		};
	$start_from = ($page-1) * $results_per_page;

	if((isset($_POST['sort_var']))){
		$sort_variable = $_POST['sort_var'];	
		//$sortType = $_POST['sort_type'];
	}
				
	?>
<div class="container mb-5">
	<div class="container-fluid pt-3">


	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				 	 <a class="navbar-brand" href="#">Search <i class="fas fa-search mt-2 ml-1 fa-lg"></i></a>
					 <a class="navbar-brand" href="#">& Sort <i class="fas fa-random mt-2 ml-1 fa-lg"></i></a>
					  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>
					  <div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto justify-content-end">
					  
					  
					   <li class="nav-item">
							<form method="POST" action="knowncargosearch.php">
								 <input class="form-control mr-sm-2" name="search_Query" type="search" value="<?php if(isset($_SESSION['Query_Search']))echo $_SESSION['Query_Search'];?>" placeholder="Search..." aria-label="Search" pattern=".{3,}" title="Enter 3 or more characters to continue search." autofocus />
								<input type="hidden" name="sort_type" value="ASC" />
							
						 </li>
					    <li class="nav-item">
					    	<button class="btn btn-outline-primary btn-block my-2 my-sm-0 mb-sm-1" name="search_Query_Submit" type="submit">Search<i class="fas fa-angle-right mt-2 ml-1"></i></button>
							</li>
					  </form>
					  
					  
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-left border-right btn btn-link nav-link" name="sort_var" value="entry_datetime ASC"><span class="text-dark">Date<i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						 </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="entry_datetime DESC"><span class="text-dark">Date<i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="vehicle_registration ASC"><span class="text-dark">vehicle_registration <i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						  </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="vehicle_registration DESC"><span class="text-dark">vehicle_registration <i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						</ul>
					  </div>
					</nav>
	
 <?php //Building the Query from View base code
 $qry = "select 
	kc.entry_datetime AS 'Date & Time',
	kc.cargo AS 'Cargo',
	kc.clearing_agent_name AS 'Clearing Agent',
	kc.clearing_agent_phone AS 'Clearing Agent Phone Number',
	kc.clearing_company AS 'Clearing Company',
	kc.driver_name AS 'Driver Name',
	kc.driver_mobile AS 'Driver Phone Number',
	kc.goods_for AS 'Client Company',
	kc.transporting_company AS 'Transporting Company',
	kc.vehicle_registration AS 'Vehicle Registration',
	kc.pass_card_number AS 'Pass Card',
	kc.security_name AS 'Security Officer',
	kc.shipping_company AS 'Shipping Company',
	case when kc.Perishable = 1 then 'YES' else 'NO' end AS 'Perishable' 
	from siginon.kc
	ORDER BY 'Date & Time' DESC";
 $result = $conn->prepare($qry);
$result->execute();
?>
		<table class="table">
		  <thead>
			<tr>
				<th scope="col">Date & Time</th>
				<th scope="col">Cargo</th>
				<th scope="col">Clearing Agent</th>
				<th scope="col">Clearing Agent Phone Number</th>
				<th scope="col">Clearing Company</th>
				<th scope="col">Driver Name</th>
				<th scope="col">Driver Phone Number</th>
				<th scope="col">Client Company</th>
				<th scope="col">Transporting Company</th>
				<th scope="col">Vehicle Registration</th>
				<th scope="col">Pass Card</th>
				<th scope="col">Security Officer</th>
				<th scope="col">Shipping Company</th>
				<th scope="col">Perishable</th>				
			</tr>
		  </thead>
		  <tbody>
		  <?php
		  $counter = 1;
		  while($got = $result->fetch()){	
		  ?>		  
			<tr>
			<th scope="row"><?php echo $counter;?></th>
				<td><?php echo $got['Date & Time'];?></td>
				<td><?php echo $got['Cargo'];?></td>
				<td><?php echo $got['Clearing Agent'];?></td>
				<td><?php echo $got['Clearing Agent Phone Number'];?></td>
				<td><?php echo $got['Clearing Company'];?></td>
				<td><?php echo $got['Driver Name'];?></td>
				<td><?php echo $got['Driver Phone Number'];?></td>
				<td><?php echo $got['Client Company'];?></td>
				<td><?php echo $got['Transporting Company'];?></td>
				<td><?php echo $got['Vehicle Registration'];?></td>
				<td><?php echo $got['Pass Card'];?></td>
				<td><?php echo $got['Security Officer'];?></td>
				<td><?php echo $got['Shipping Company'];?></td>
				<td><?php echo $got['Perishable'];?></td>
				
			<form action="" method="POST" class="footer_list">
			<!-- <input type="hidden" name="product_Query_Page" value="<?php //echo $got['known_cargo_acceptance_ID'] ?>"/> -->

			</form></td>
			</tr>
		  <?php
		  $counter = $counter + 1; 
		  }
			  ?>
		  </tbody>
		  </table>
		  
		  <?php
	
	
	$query = $conn->prepare(  "select 
	kc.entry_datetime AS 'Date & Time',
	kc.cargo AS 'Cargo',
	kc.clearing_agent_name AS 'Clearing Agent',
	kc.clearing_agent_phone AS 'Clearing Agent Phone Number',
	kc.clearing_company AS 'Clearing Company',
	kc.driver_name AS 'Driver Name',
	kc.driver_mobile AS 'Driver Phone Number',
	kc.goods_for AS 'Client Company',
	kc.transporting_company AS 'Transporting Company',
	kc.vehicle_registration AS 'Vehicle Registration',
	kc.pass_card_number AS 'Pass Card',
	kc.security_name AS 'Security Officer',
	kc.shipping_company AS 'Shipping Company',
	case when kc.Perishable = 1 then 'YES' else 'NO' end AS 'Perishable'  
			from `siginon`.`known_cargo`;");
		  //$sql = "SELECT userId AS total FROM users ";
    //  $query = $conn->prepare($sql);

      $query ->execute();
      $row = $query->rowCount();
	
	$total_pages = ceil($row / $results_per_page); // calculate total pages with results

for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
         echo " <a href='knowncargo.php?page=".$i."'";

            if ($i==$page)  echo " class='curPage'";
            echo ">".$i."</a> "; 
	
	
}
		?>
	</div>
</div>




<?php
	
include_once('footer.php');
}
else{
	header('Location: login.php');
}
?>
