<?php
ob_start();
include_once('header.php');
/*Home page*/
$page='Export Cargo Delivery View';
$sort_variable = "entry";
include_once 'includes/db_connect.php';
if($_SESSION['isadminloggedin']==1){

?>


	<?php

	$results_per_page = 20;

	if (isset($_GET["page"])) 
		{
			$page  = $_GET["page"];
		} 
	else 
		{ 
			$page=1;
		};
	$start_from = ($page-1) * $results_per_page;

	if((isset($_POST['sort_var'])))
	{
		$sort_variable = $_POST['sort_var'];	
		//$sortType = $_POST['sort_type'];
	}

		
	?>
<div class="container mb-5">
	<div class="container-fluid pt-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="exportcargodeliverysearch.php">Home</a></li>
		<li class="breadcrumb-item">Search results</li>
	  </ol>
	</nav>
	
	
	<?php
		if(isset($message)){ echo '<div class="alert alert-primary" role="alert">
		  <strong>Note: </strong>
			' .$message. '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
  </button></div>'; }
		?>
</div>
	<?php //echo '<h3 class="font-weight-light">' .$count. ' results found! for your query "'.$var1.'"</h3><br/>';?>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				 	 <a class="navbar-brand" href="#">Search <i class="fas fa-search mt-2 ml-1 fa-lg"></i></a>
					 <a class="navbar-brand" href="#">& Sort <i class="fas fa-random mt-2 ml-1 fa-lg"></i></a>
					  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>
					  <div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto justify-content-end">
					  
					  
					   <li class="nav-item">
							<form method="POST" action="exportcargodeliverysearch.php">
								 <input class="form-control mr-sm-2" name="search_Query" type="search" value="<?php if(isset($_SESSION['Query_Search']))echo $_SESSION['Query_Search'];?>" placeholder="Search..." aria-label="Search" pattern=".{3,}" title="Enter 3 or more characters to continue search." autofocus />
								<input type="hidden" name="sort_type" value="ASC" />
							
						 </li>
					    <li class="nav-item">
					    	<button class="btn btn-outline-primary btn-block my-2 my-sm-0 mb-sm-1" name="search_Query_Submit" type="submit">Search<i class="fas fa-angle-right mt-2 ml-1"></i></button>
							</li>
					  </form>
					  
					  
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-left border-right btn btn-link nav-link" name="sort_var" value="entry_datetime ASC"><span class="text-dark">Date<i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						 </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="entry_datetime DESC"><span class="text-dark">Date<i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="AWB ASC"><span class="text-dark">AWB<i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						  </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="AWB DESC"><span class="text-dark">AWB<i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						</ul>
					  </div>
					</nav>
	
<?php // Query built from View base code
		$qry = "e.entry AS 'Entry Date & Time',
				e.AWB AS 'AWB',
				e.goods AS 'Goods',
				e.pieces AS 'Pieces',
				e.location_abbr AS 'Current Location',
				e.weight AS 'Weight(Kg)',
				e.destination_abbr AS 'Destination',
				e.clearing_agent_company AS 'Clearing Agent Company',
				e.clearing_agent_name AS 'Clearing Agent Name' ,
				e.clearing_agent_govt_ID AS 'ID Number',
				e.clearing_agent_phone AS 'Phone Number',
				e.flight AS 'Flight',
				e.ULD AS 'ULD/Container',
				e.TimeOut AS 'Exit Date & Time',
				e.export_driver_name AS 'Export Driver Name',
				e.export_driver_govt_ID AS 'Export Driver ID',
				e.pass_card AS 'Pass Card',
				e.clerk_name AS 'Siginon Staff',
				e.security_name AS 'Security Name'
				from siginon.export_cargo_delivery AS e";
		$result = $conn->prepare($qry);
		$result->execute();	
?>		
		<table class="table">
			<thead>
				<tr>
					<th scope="col">Entry Date & Time</th>
					<th scope="col">AWB</th>
					<th scope="col">Goods</th>
					<th scope="col">Pieces</th>
					<th scope="col">Current Location</th>
					<th scope="col">Weight(Kg)</th>
					<th scope="col">Destination</th>
					<th scope="col">Clearing Agent Company</th>
					<th scope="col">Clearing Agent Name</th>
					<th scope="col">ID Number</th>
					<th scope="col">Phone Number</th>
					<th scope="col">Flight</th>
					<th scope="col">ULD/Container</th>
					<th scope="col">Exit Date</th>
					<th scope="col">Export Driver Name</th>
					<th scope="col">Export Driver ID</th>
					<th scope="col">Pass Card</th>
					<th scope="col">Siginon Staff</th>		
					<th scope="col">Security Name</th>		
				</tr>
			</thead>
			<tbody>
				<?php
					$counter = 1;
					while($got = $result->fetch())
					{	
				?>
					<tr>
						<th scope="row"><?php echo $counter;?></th>
							
							<td><?php echo $got['Entry Date & Time'];?></td>
							<td><?php echo $got['AWB'];?></td>
							<td><?php echo $got['Goods'];?></td>
							<td><?php echo $got['Pieces'];?></td>
							<td><?php echo $got['Current Location'];?></td>
							<td><?php echo $got['Weight(Kg)'];?></td>
							<td><?php echo $got['Destination'];?></td>
							<td><?php echo $got['Clearing Agent Company'];?></td>
							<td><?php echo $got['Clearing Agent Name'];?></td>
				            <td><?php echo $got['Phone Number'];?></td>
							<td><?php echo $got['ID Number'];?></td>
							<td><?php echo $got['Flight'];?></td>
							<td><?php echo $got['ULD/Container'];?></td>
							<td><?php echo $got['Exit Date & Time'];?></td>
							<td><?php echo $got['Export Driver Name'];?></td>
							<td><?php echo $got['Export Driver ID'];?></td>
							<td><?php echo $got['Pass Card'];?></td>
							<td><?php echo $got['Siginon Staff'];?></td>
							<td><?php echo $got['Security Name'];?></td>
							<td>
								<form action="" method="POST" class="footer_list">
									<input type="hidden" name="product_Query_Page" value="<?php echo $got['export_cargo_ID'] ?>"/>
								</form>
							</td>
					</tr>
				<?php
						$counter = $counter + 1;
					}
				?>
			</tbody>
		</table>
		  
		  
		  
		  <?php
	
      $row = $look_for_search->rowCount();
	
	$total_pages = ceil($row / $results_per_page); // calculate total pages with results
  
for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
         echo " <a href='exportcargo.php?page=".$i."'";

            if ($i==$page)  echo " class='curPage'";
            echo ">".$i."</a> "; 
	
	
}
		?>
	</div>
</div>




<?php
	
include_once('footer.php');
}

else{
	header('Location: login.php');
}
?>
