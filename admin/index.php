<?php
ob_start();
error_reporting(E_ALL);
include_once('header.php');
/*Home page*/
$page='Uknown Cargo View';
$sort_variable = "entry_time";
include_once 'includes/db_connect.php';
if($_SESSION['isadminloggedin']==1){

?>

	<?php

		
	$results_per_page = 20;

	if (isset($_GET["page"])) 
		{
			$page  = $_GET["page"];
		} 
	else 
		{ 
			$page=1;
		};
	$start_from = ($page-1) * $results_per_page;

	if((isset($_POST['sort_var'])))
	{
		$sort_variable = $_POST['sort_var'];	
		//$sortType = $_POST['sort_type'];
	}	
	?>
	
<div class="container mb-5">
	<div class="container-fluid pt-3">	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				 	 <a class="navbar-brand" href="#">Search <i class="fas fa-search mt-2 ml-1 fa-lg"></i></a>
					 <a class="navbar-brand" href="#">& Sort <i class="fas fa-random mt-2 ml-1 fa-lg"></i></a>
					  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>
					  <div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto justify-content-end">
					  
					  
					   <li class="nav-item">
							<form method="POST" action="searchresults.php">
								 <input class="form-control mr-sm-2" name="search_Query" type="search" value="<?php if(isset($_SESSION['Query_Search']))echo $_SESSION['Query_Search'];?>" placeholder="Search..." aria-label="Search" pattern=".{3,}" title="Enter 3 or more characters to continue search." autofocus />
								<input type="hidden" name="sort_type" value="ASC" />
							
						 </li>
					    <li class="nav-item">
					    	<button class="btn btn-outline-primary btn-block my-2 my-sm-0 mb-sm-1" name="search_Query_Submit" type="submit">Search<i class="fas fa-angle-right mt-2 ml-1"></i></button>
							</li>
					  </form>
					  
					  
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-left border-right btn btn-link nav-link" name="sort_var" value="entry_time ASC"><span class="text-dark">Date<i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						 </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="entry_time DESC"><span class="text-dark">Date<i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="pieces ASC"><span class="text-dark">Pieces <i class="fas fa-sort-numeric-down mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="ASC" />
							</form>
						  </li>
						  <li class="nav-item">
							<form method="POST">
								<button type="submit" class="border-right btn btn-link nav-link" name="sort_var" value="pieces DESC"><span class="text-dark">Pieces <i class="fas fa-sort-numeric-up mt-2 ml-1"></i></span></button>
								<input type="hidden" name="sort_type" value="DESC" />
							</form>
						  </li>
						</ul>
					  </div>
					</nav>

<?php 
$qry = "select
	ukc.entry_time AS 'Date & Time',
	ukc.vehicle_registration AS 'Vehicle Registration',
	ukc.driver_name AS 'Driver Name',
	ukc.driver_mobile AS 'Driver Phone',
	ukc.passcard AS 'Pass Issued',
	ukc.transporting_company AS 'Transporting Company',
	ukc.client_company AS 'Client Company',
	ukc.cargo AS 'Cargo',
	ukc.pieces AS 'Pieces',
	ukc.seal_number AS 'Seal Number',
	ukc.security_name AS 'Security Name',
	ukc.shipping_agent_name AS 'Shipping Agent Name',
	ukc.shipping_agent_phone AS 'Shipping Agent Phone',
	ukc.shipping_company AS 'Shipping Company',
	case when ukc.cargo_accepted_status = 1 then 'YES' else 'NO' end AS 'Accepted',
	case when ukc.Perishable = 1 then 'YES' else 'NO' end AS 'Perishable' 
	from siginon.unknown_cargo AS ukc
	ORDER BY 'Date & Time' DESC";
$result = $conn->prepare($qry);
$result->execute();
?>
		<table class="table">
			<thead>
				<tr>
				<th scope="col">No.</th>
					<th scope="col">Date $ Time</th>
					<th scope="col">Vehicle</th>
					<th scope="col">Driver Name</th>
					<th scope="col">Driver Phone Number</th>
					<th scope="col">Pass Issued</th>
					<th scope="col">Transporting Company</th>
					<th scope="col">Client Company</th>
					<th scope="col">Cargo</th>
					<th scope="col">Pieces</th>
					<th scope="col">Seal Number</th>
					<th scope="col">Security Name</th>
					<th scope="col">Shipping Agent Name</th>
					<th scope="col">Shipping Agent Phone</th>
					<th scope="col">Shipping Company</th>
					<th scope="col">Accepted?</th>
					<th scope="col">Perishable?</th>					
				</tr>
			</thead>
			<tbody>
				<?php
					$counter = 1;
					while($got = $result->fetch())
					{	
				?>
				<tr>
					<th scope="row"><?php echo $counter;?></th>
					<td><?php echo $got['Date & Time'];?></td>
					<td><?php echo $got['Vehicle Registration'];?></td>
					<td><?php echo $got['Driver Name'];?></td>]
					<td><?php echo $got['Driver Phone'];?></td>
					<td><?php echo $got['Pass Issued'];?></td>
					<td><?php echo $got['Transporting Company'];?></td>
					<td><?php echo $got['Client Company'];?></td>
					<td><?php echo $got['Cargo'];?></td>
					<td><?php echo $got['Pieces'];?></td>
					<td><?php echo $got['Seal Number'];?></td>
					<td><?php echo $got['Security Name'];?></td>
					<td><?php echo $got['Shipping Agent Name'];?></td>
					<td><?php echo $got['Shipping Agent Phone'];?></td>
					<td><?php echo $got['Shipping Company'];?></td>
					<td><?php echo $got['Accepted'];?></td>
					<td><?php echo $got['Perishable'];?></td>
					
					<!-- <td><form action="" method="POST" class="footer_list"><input type="hidden" name="product_Query_Page" value="<?php //echo $got['unknown_cargo_ID'] ?>"/> </form>	</td> -->
				</tr>
				<?php
				$counter = $counter + 1; 
				}
				?>
			</tbody>
		</table>
		  <?php
	
	
	$query = $conn->prepare("select
	ukc.entry_time AS 'Date & Time',
	ukc.vehicle_registration AS 'Vehicle Registration',
	ukc.driver_name AS 'Driver Name',
	ukc.driver_mobile AS 'Driver Phone',
	ukc.passcard AS 'Pass Issued',
	ukc.transporting_company AS 'Transporting Company',
	ukc.client_company AS 'Client Company',
	ukc.cargo AS 'Cargo',
	ukc.pieces AS 'Pieces',
	ukc.seal_number AS 'Seal Number',
	ukc.security_name AS 'Security Name',
	ukc.shipping_agent_name AS 'Shipping Agent Name',
	ukc.shipping_agent_phone AS 'Shipping Agent Phone',
	ukc.shipping_company AS 'Shipping Company',
	case when ukc.cargo_accepted_status = 1 then 'YES' else 'NO' end AS 'Accepted',
	case when ukc.Perishable = 1 then 'YES' else 'NO' end AS 'Perishable'  
		from `siginon`.`unknown_cargo`;");
		  //$sql = "SELECT userId AS total FROM users ";
    //  $query = $conn->prepare($sql);


      $query ->execute();
      $row = $query->rowCount();
	
	$total_pages = ceil($row / $results_per_page); // calculate total pages with results

for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
         echo " <a href='index.php?page=".$i."'";

            if ($i==$page)  echo " class='curPage'";
            echo ">".$i."</a> "; 
	
	
}
		?>
	</div>
</div>




<?php
	
include_once('footer.php');
}
else{
	header('Location: login.php');
}
?>
