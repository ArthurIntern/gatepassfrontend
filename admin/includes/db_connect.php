<?php
include_once 'psl-config.php';
try {
	 
    $conn = new PDO("mysql:host=$databaseHost;port=3306;dbname=$databaseName", $databaseUsername, $databasePassword);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
} catch(PDOException $e) {
    echo $e->getMessage();
}
?>
