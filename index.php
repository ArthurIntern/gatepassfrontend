<?php
ob_start();
//Home page aka landing page
$page="Home";
include_once('header.php');
error_reporting(0);


//$_SESSION['isloggedin']=1;
?>
<?php

include_once 'includes/db_connect.php';

if($_SESSION['isloggedin']==1){
?>
<div class="container ">
	<div class="container-fluid pt-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="index.php" class="text-danger">Home</a></li>
		<li class="breadcrumb-item text-danger">Dashboard</li>
	  </ol>
	</nav>
	<?php
	if(isset($_SESSION['message'])){ echo '<div class="alert mt-3 alert-success" role="alert">
			  <strong>Note: </strong>
				'.$_SESSION['message'].'<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	  </button></div>'; }
	   unset($_SESSION['message']);
	
if(isset($_SESSION['error'])){ echo '<div class="alert mt-3 alert-danger" role="alert">
			  <strong>Error: </strong>
				'.$_SESSION['error'].'<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	  </button></div>'; }
	   unset($_SESSION['error']);



	?>
		<div class="text-center row">
			
				
			<div class="col-md-4 ">
				<div class="p-2 mt-4   border rounded boxprofile ">
					<h4 ><i class="fas fa-search-plus   fa-lg mr-2 mt-4 text-danger "></i><span class="text-danger">Unknown Cargo</span></h4>
					<li class="list-unstyled simple_link"><a href="unknown-cargo.php" class="text-dark">Unknown Cargo</a></li>
				</div>
				
			</div>
			
			
			
					
			<div class="col-md-4  ">
				<div class="p-2 mt-4 border rounded boxprofile">
					
					<h4><i class="fas fa-shopping-basket fa-lg mr-2 mt-4 text-danger"></i><span class="text-danger">Known Cargo<a href=""></a></span></h4>
					<li class="list-unstyled simple_link"><a href="known-cargo.php" class="text-dark">Known Cargo</a></li>
				
				
				</div>
			</div>
			

			
			
			<div class="col-md-4">
				<div class="p-2 mt-4 border rounded boxprofile">
					
				
				
					<h4><i class="fas fa-street-view fa-lg mr-2 mt-4 text-danger"></i><span class="text-danger">General Cargo Export</a></span></h4>
					<li class="list-unstyled simple_link"><a href="general-cargo-export.php" class="text-dark">General Cargo Export</a></li>
				
				

				</div>
			</div>


			<div class="col-md-4">
				<div class="p-2 mt-4 border rounded boxprofile">
					<h4><i class="fas fa-credit-card fa-lg mr-2 mt-4 text-danger"></i><span class="text-danger">Export Cargo Delivery</span></h4>
					<li class="list-unstyled simple_link"><a href="export-cargo-delivery.php" class="text-dark">Export Cargo Delivery</a></li>
				</div>
			</div>
			

			
				<div class="col-md-4">
				<div class="p-2 mt-4 border rounded boxprofile">
											
					<h4><i class="fas fa-check-square fa-lg mr-2 mt-4 text-danger"></i><span class="text-danger">Import Delivery</span></h4>
					<li class="list-unstyled simple_link"><a href="import-delivery.php" class="text-dark">Import Delivery</a></li>
		
					
				</div>
			</div>





			
			<div class="col-md-4">
				<div class="p-2 mt-4 border rounded boxprofile">
					<h4><i class="fas fa-lock fa-lg mr-2 mt-4 text-danger"></i><span class="text-danger">Login & Security</span></h4>
					<li class="list-unstyled simple_link"><a href="change_details.php" class="text-dark">Change Password, Email, Phone</a></li>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
} 
else{

 	echo "<script>alert('Please Log in first');</script>";


	header('Location: home.php');
}
?>
<?php
include_once('footer.php');
?>
