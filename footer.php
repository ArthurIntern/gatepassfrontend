</div>

<hr/>
<div class="container-fluid text-danger">
	<div class="row text-center p-3">
		<div class="col-sm-6 col-md-3 mb-3 border-right">
			<span class="form-inline"><i class="fas fa-eye fa-2x mr-2"></i><h5 class="mt-sm-1">Secure</h5></span>
		</div>
		<div class="col-sm-6 col-md-3 mb-3 border-right">
			<span class="form-inline"><i class="fas fa-user-secret fa-2x mr-2"></i><h5 class="mt-sm-1">Efficient</h5></span>
		</div>
		<div class="col-sm-6 col-md-3 mb-3 border-right">
			<span class="form-inline"><i class="fas fa-redo fa-2x mr-2"></i><h5 class="mt-sm-1">Easy</h5></span>
		</div>
		<div class="col-sm-6 col-md-3 mb-3 border-right">
			<span class="form-inline"><i class="fas fa-lock fa-2x mr-2"></i><h5 class="mt-sm-1">Guaranteed Security</h5></span>
		</div>
	</div>
</div>
<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
    <script src="js/popper.min.js" crossorigin="anonymous"></script>
	<script src="js/bootstrap.min.js" crossorigin="anonymous"></script>
  </body>
</html>