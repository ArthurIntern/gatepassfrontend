<?php
include_once 'psl-config.php';

try {

    $host ="mysql:host=$databaseHost;port=3306;dbname=$databaseName;charset=UTF8";

  $conn = new PDO($host, $databaseUsername, $databasePassword);
    
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
} catch(PDOException $e) {
    echo $e->getMessage();
}
 
?>
